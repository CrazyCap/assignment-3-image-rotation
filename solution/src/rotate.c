#include "image.h"
#include "rotate.h"
#include <stdlib.h>

const struct image invalid_image = {0, 0, NULL};
void free_image(struct image* img) {
    if (img->data) {
        free(img->data);
        img->data = NULL;
    }
}
void calculate_rotated_dimensions(const struct image* input, int angle, uint64_t* rotated_width, uint64_t* rotated_height) {
    angle %= 360;
    if (angle == 90 || angle == -270 || angle == 270 || angle == -90) {
        *rotated_width = input->height;
        *rotated_height = input->width;
    } else {
        *rotated_width = input->width;
        *rotated_height = input->height;
    }
}
struct pixel get_rotated_pixel(const struct image* input, int angle, uint64_t x, uint64_t y, uint64_t rotated_width, uint64_t rotated_height) {
    uint64_t src_x, src_y;

    if (angle == -90 || angle == 270) {
        src_x = y;
        src_y = rotated_width - 1 - x;
    } else if (angle == 180 || angle == -180) {
        src_x = rotated_width - 1 - x;
        src_y = rotated_height - 1 - y;
    } else if (angle == 90 || angle == -270) {
        src_x = rotated_height - 1 - y;
        src_y = x;
    } else {
        src_x = x;
        src_y = y;
    }

    if (src_x < input->width && src_y < input->height) {
        return input->data[src_y * input->width + src_x];
    } else {
        return (struct pixel) {0, 0, 0};
    }
}

struct image rotate_image(const struct image* input, int angle) {
    if (!input || input->width <= 0 || input->height <= 0 || !input->data) {
        return invalid_image;
    }

    uint64_t rotated_width, rotated_height;
    calculate_rotated_dimensions(input, angle, &rotated_width, &rotated_height);

    struct image output = {rotated_width, rotated_height, NULL};

    if (rotated_width > 0 && rotated_height > 0) {
        output.data = (struct pixel *) calloc(rotated_width * rotated_height, sizeof(struct pixel));
        if (!output.data) {
            return invalid_image;
        }
    } else {
        return invalid_image;
    }

    for (uint64_t y = 0; y < rotated_height; ++y) {
        for (uint64_t x = 0; x < rotated_width; ++x) {
            output.data[y * rotated_width + x] = get_rotated_pixel(input, angle, x, y, rotated_width, rotated_height);
        }
    }

    return output;
}
