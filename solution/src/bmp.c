#include "bmp.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#define BIT_COUNT 24
long calculate_padding(uint64_t width) {
    long row_size = ((long)width * BIT_COUNT + 7) / 8;
    return (4 - (row_size % 4)) % 4;
}
enum read_status check_bmp_header(const struct bmp_header* header) {
    if (!header) {
        return READ_INVALID_HEADER;
    }

    if (header->bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    if (header->biWidth <= 0 || header->biHeight <= 0) {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum read_status from_bmp(FILE* in_file, struct image* img) {
    if (!in_file || !img) {
        return READ_FILE_ERROR;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in_file) != 1) {
        return READ_INVALID_HEADER;
    }

    enum read_status header_status = check_bmp_header(&header);
    if (header_status != READ_OK) {
        return header_status;
    }

    // Выделение памяти и чтение данных происходит только после успешной проверки заголовка
    img->width = header.biWidth;
    img->height = header.biHeight;

    size_t padding = calculate_padding(img->width);
    size_t row_size = ((size_t)img->width * 3 + padding);
    size_t image_size = row_size * img->height;

    img->data = (struct pixel*)malloc(image_size);
    if (!img->data) {
        return READ_INVALID_HEADER;
    }

    if (fseek(in_file, (long)header.bOffBits, SEEK_SET) != 0) {
        free(img->data);
        return READ_INVALID_HEADER;
    }

    for (uint64_t i = 0; i < img->height; ++i) {
        if (fread(img->data + i * img->width, 3, img->width, in_file) != img->width) {
            free(img->data);
            return READ_INVALID_HEADER;
        }
        fseek(in_file, (long)padding, SEEK_CUR);
    }

    return READ_OK;
}

void create_bmp_header(struct bmp_header* header, const struct image* img) {
    if (!header || !img) {
        return;
    }

    long padding = calculate_padding(img->width);
    size_t row_size = ((size_t)img->width * 3 + padding);
    size_t image_size = row_size * img->height;

    header->bfType = 0x4D42; // 'BM'
    header->bfileSize = sizeof(struct bmp_header) + image_size;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);

    header->biSize = 40;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = 1;
    header->biBitCount = 24;
    header->biCompression = 0;
    header->biSizeImage = image_size;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}

enum write_status to_bmp(FILE* out_file, const struct image* img) {
    if (!out_file || !img || img->width <= 0 || img->height <= 0 || !img->data) {
        return WRITE_ERROR;
    }

    struct bmp_header header;
    create_bmp_header(&header, img);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out_file) != 1) {
        return WRITE_ERROR;
    }

    long padding = calculate_padding(img->width);

    for (uint64_t y = 0; y < img->height; ++y) {
        if (fwrite(&img->data[y * img->width], sizeof(struct pixel), img->width, out_file) != img->width) {
            return WRITE_ERROR;
        }

        for (long p = 0; p < padding; ++p) {
            if (fputc(0, out_file) == EOF) {
                return WRITE_ERROR;
            }
        }
    }

    if (fflush(out_file) != 0) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
