#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct image source_img;
    FILE* source_file = fopen(argv[1], "rb");
    if (!source_file) {
        perror("Failed to open source image file");
        return EXIT_FAILURE;
    }

    enum read_status read_result = from_bmp(source_file, &source_img);
    fclose(source_file);

    if (read_result != READ_OK) {
        fprintf(stderr, "Error reading the source image: %d\n", read_result);
        free_image(&source_img);
        return EXIT_FAILURE;
    }

    char* endptr;
    int angle = (int)strtol(argv[3], &endptr, 10);

    if (*endptr != '\0') {
        fprintf(stderr, "Ошибка при преобразовании угла: %s не является целым числом\n", argv[3]);
        return 1;
    }
    if (angle % 90 != 0 || (angle != 0 && angle != 90 && angle != -90 && angle != 180 && angle != -180 && angle != 270 && angle != -270)) {
        fprintf(stderr, "Invalid rotation angle. Valid angles are 0, 90, -90, 180, -180, 270, -270.\n");
        free_image(&source_img);
        return EXIT_FAILURE;
    }

    struct image transformed_img = rotate_image(&source_img, angle);

    FILE* transformed_file = fopen(argv[2], "wb");
    if (!transformed_file) {
        perror("Failed to open transformed image file");
        free_image(&source_img);
        free_image(&transformed_img);
        return EXIT_FAILURE;
    }


    enum write_status write_result = to_bmp(transformed_file, &transformed_img);
    fclose(transformed_file);

    if (write_result != WRITE_OK) {
        fprintf(stderr, "Error writing the transformed image: %d\n", write_result);
        free_image(&source_img);
        free_image(&transformed_img);
        return EXIT_FAILURE;
    }
    free_image(&source_img);
    free_image(&transformed_img);
    printf("Image transformation successful.\n");
    return EXIT_SUCCESS;
}
