#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct image;

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
};

enum rotation_result {
    ROTATION_OK,
};

void create_bmp_header(struct bmp_header* header, const struct image* img);
enum read_status from_bmp(FILE* in, struct image* img);
enum read_status check_bmp_header(const struct bmp_header* header);
enum write_status to_bmp(FILE* out, struct image const* img);
void free_image(struct image* img);
#endif // IMAGE_TRANSFORMER_BMP_H
