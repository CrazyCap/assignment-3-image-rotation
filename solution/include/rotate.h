#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"

struct image rotate_image(const struct image* input, int angle);

#endif // IMAGE_TRANSFORMER_ROTATE_H
